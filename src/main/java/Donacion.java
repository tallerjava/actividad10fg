
import java.text.DecimalFormat;

public class Donacion implements Comparable<Donacion> {

    String pais;
    double monto;

    public Donacion(String pais, double monto) {
        this.pais = pais;
        this.monto = monto;
    }

    public String getPais() {
        return pais;
    }

    public double getMonto() {
        return monto;
    }

    @Override
    public String toString() {
        DecimalFormat formatoDecimal = new DecimalFormat(".##");
        return "Pais: " + pais + " Monto: " + formatoDecimal.format(monto);
    }

    @Override
    public int compareTo(Donacion donacion) {
        if (monto < donacion.monto) {
            return -1;
        } else if (monto > donacion.monto) {
            return 1;
        } else {
            return 0;
        }
    }
}
