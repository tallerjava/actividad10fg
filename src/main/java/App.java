
import java.io.IOException;

public class App {

    public static void main(String[] args) throws IOException {
        try {
            if (args != null || !args[0].isEmpty()) {
                DonacionRepository donacionRepository = new DonacionRepository(args[0]);
                ReporteService repoService = new ReporteService(donacionRepository);
                Reporte reporte = repoService.calcularRecaudacion();
                reporte.ordenarAscendentementeRecaudacionPorPais();
                System.out.println("Recaudación por País:");
                reporte.getRecaudacionPorPais().forEach(System.out::println);
                System.out.println("\nRecaudación general: " + reporte.recaudacionGeneral);
            } else {
                throw new IllegalArgumentException("Se requiere el ingreso de un valor como argumento para iniciar el programa.");
            }
        } catch (IOException exception) {
            System.out.println(exception);
        }
    }
}
