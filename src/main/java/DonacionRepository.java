
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class DonacionRepository {

    private String nombreArchivo;

    public DonacionRepository(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public ArrayList<Donacion> listarDonacion() throws IOException {
        ArrayList<Donacion> listado = new ArrayList();
        File archivo = new File(nombreArchivo);
        BufferedReader in = new BufferedReader(new FileReader(archivo));
        if (!archivo.exists()) {
            throw new FileNotFoundException("Archivo no encontrado");
        } else {
            String linea;
            while ((linea = in.readLine()) != null) {
                String[] valores = linea.split("\\|");
                String pais = valores[1];
                double monto = Double.parseDouble(valores[6].replaceAll("\\$", ""));
                listado.add(new Donacion(pais, monto));
            }
        }
        return listado;
    }
}
