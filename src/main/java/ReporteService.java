
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.math.BigDecimal;

public class ReporteService {

    private DonacionRepository donacionRepository;

    public ReporteService(DonacionRepository donacionRepository) {
        this.donacionRepository = donacionRepository;
    }

    public Reporte calcularRecaudacion() throws IOException {
        ArrayList<Donacion> listado = donacionRepository.listarDonacion();
        HashMap<String, Double> recaudacionPorPais = new HashMap<>();
        BigDecimal montoTotal = BigDecimal.ZERO;
        for (Donacion donacion : listado) {
            if (recaudacionPorPais.containsKey(donacion.getPais())) {
                BigDecimal montoActualPais = new BigDecimal(recaudacionPorPais.get(donacion.getPais()));
                BigDecimal nuevoMontoPais = montoActualPais.add(new BigDecimal(donacion.getMonto()));
                recaudacionPorPais.put(donacion.getPais(), nuevoMontoPais.doubleValue());
            } else {
                recaudacionPorPais.put(donacion.getPais(), donacion.getMonto());
            }
            BigDecimal nuevoMontoTotal = montoTotal.add(new BigDecimal(donacion.getMonto()));
            montoTotal = nuevoMontoTotal;
        }
        double recaudacionGeneral = montoTotal.doubleValue();
        Reporte reporte = new Reporte(recaudacionPorPais, recaudacionGeneral);
        return reporte;
    }
}
