
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class Reporte {

    List<Donacion> recaudacionPorPais = new ArrayList();
    double recaudacionGeneral;

    Reporte(HashMap<String, Double> recaudacionPorPais, double recaudacionGeneral) {
        for (Map.Entry<String, Double> donacion : recaudacionPorPais.entrySet()) {
            this.recaudacionPorPais.add(new Donacion(donacion.getKey(), donacion.getValue()));
        }
        this.recaudacionGeneral = recaudacionGeneral;
    }

    public List<Donacion> getRecaudacionPorPais() {
        return recaudacionPorPais;
    }

    public double getRecaudacionGeneral() {
        return recaudacionGeneral;
    }

    public void ordenarDescendentementeRecaudacionPorPais() {
        Collections.sort(recaudacionPorPais);
    }

    public void ordenarAscendentementeRecaudacionPorPais() {
        Collections.sort(recaudacionPorPais, Collections.reverseOrder());
    }
}
