
import org.junit.Test;
import static org.junit.Assert.*;

public class DonacionTest {
       
    @Test
    public void getMonto_ValoresValidosObjetoDonacion_obtencionDeMonto() {
        double resultadoEsperado = 100.09;
        Donacion donacionInstancia = new Donacion("Canada", resultadoEsperado);
        double resultadoObtenido = donacionInstancia.getMonto();
        assertEquals(resultadoEsperado, resultadoObtenido, 0.001);
    }
    
    @Test
    public void getPais_ValoresValidosObjetoDonacion_obtencionDePais() {
        String resultadoEsperado = "Canada";
        Donacion donacionInstancia = new Donacion(resultadoEsperado, 100.10);
        String resultadoObtenido = donacionInstancia.getPais();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    @Test
    public void toString_ValoresValidosObjetoDonacion_stringCorrecto() {
        String resultadoEsperado = "Pais: Canada Monto: 100,01";
        Donacion donacionInstancia = new Donacion("Canada", 100.009f);
        String resultadoObtenido = donacionInstancia.toString();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    @Test 
    public void compareTo_MontoIngresadoMenorAMontoActual_RetornarMenosUno(){
     Donacion donacionActual = new Donacion("Canada", 100.00);
     Donacion donacionNueva = new Donacion("Canada", 100.01);
     assertEquals(-1, donacionActual.compareTo(donacionNueva));
    }
    
    @Test 
    public void compareTo_MontoIngresadoMayorAMontoActual_RetornarUno(){
     Donacion donacionActual = new Donacion("Canada", 100.01);
     Donacion donacionNueva = new Donacion("Canada", 100.00);
     assertEquals(1, donacionActual.compareTo(donacionNueva));
    }
    
    @Test 
    public void compareTo_MontoIngresadoIgualAMontoActual_RetornarCero(){
     Donacion donacionActual = new Donacion("Canada", 100.00);
     Donacion donacionNueva = new Donacion("Canada", 100.00);
     assertEquals(0, donacionActual.compareTo(donacionNueva));
    }
}




