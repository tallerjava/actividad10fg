
import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.Test;
import static org.junit.Assert.*;

public class DonacionRepositoryTest {

    public DonacionRepositoryTest() {
    }

    @Test(expected = FileNotFoundException.class)
    public void listarDonacionTest_NombreDeArchivoIncorrecto_FileNotFoundException() throws IOException {
        DonacionRepository donacionRepository = new DonacionRepository("Donaciones");
        donacionRepository.listarDonacion();
    }

    @Test
    public void listarDonacionTest_NombreDeArchivoCorrecto_RetornarReporte() throws IOException {
        DonacionRepository donacionRepository = new DonacionRepository("Donaciones.txt");
        assertNotNull(donacionRepository.listarDonacion());
    }
}
