
import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.Test;
import static org.junit.Assert.*;

public class ReporteServiceTest {

    public ReporteServiceTest() {
    }

    @Test(expected = FileNotFoundException.class)
    public void calcularRecaudacionTest_ArgumentoInvalidoDonacionRepository_FileNotFoundException() throws IOException {
        DonacionRepository donacionRepository = new DonacionRepository("donaciones.johnson");
        ReporteService reporteService = new ReporteService(donacionRepository);
        reporteService.calcularRecaudacion();
    }

    @Test
    public void calcularRecaudacionTest_ArgumentoValidoDonacionRepository_retornarReporte() throws IOException {
        DonacionRepository donacionRepository = new DonacionRepository("donaciones.txt");
        ReporteService reporteService = new ReporteService(donacionRepository);
        assertNotNull(reporteService.calcularRecaudacion());
    }
}
